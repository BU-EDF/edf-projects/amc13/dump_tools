#ifndef __FEDHISTOGRAM_HH__
#define __FEDHISTOGRAM_HH__

//Standard includes:
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <iostream>

// Root include files:
//#include "FWCore/ServiceRegistry/interface/Service.h"
//#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
namespace FedAMC13{
  class Histogram {
  public:
    Histogram();
    ~Histogram();
   //Functions to create and write histograms
    void ConstructHistogram(int runNumber,std::vector<int> FEDID);
    void SetupHistogram(size_t BcNLow, size_t BcNHigh,uint32_t OrNLow, uint32_t OrNHigh);
    void WriteHistogram();
    //List of histograms to fill
    void Fill_BcN(std::vector<uint32_t> BcN,uint32_t BcNHigh,uint32_t BcNLow);
    void Fill_OrN(std::vector<uint32_t> OrN,uint32_t OrNHigh,uint32_t OrNLow);
    void Fill_FEDErrors(uint32_t errorState, uint32_t iFED, uint32_t iAMC, uint32_t ierror);
  private:
    TFile* rootFile;    
    TH1D * histoBcN; 
    TH1D * histoOrN;
    TH2D * FEDErrors; 
    std::string rootFileName;
  };
}
#endif
