//
// read output from HCAL dump
// write as binary deadbeef file
//
// Input format:
//    Run 228735 Event 1
//    0000 5100000143a46008
//    0001 10b064a000004960
//    0002 0f0000910002421d
//

#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#define MAX_EVT (1<<20)

void usage() {
  printf("usage:  undump_FED_raw_data_Product -f <fed> <hex_file> <bin_file\n");
}

int main( int argc, char *argv[]) {

  FILE *fpi, *fpo;
  char buff[256];
  uint64_t *d;
  uint64_t hdr[2];
  int nd = 0;

  const int max_files = 2;
  char *files[max_files];
  int num_files = 0;

  int run_no = 0;
  int fed_no, fed_size, evn, bcn;
  int fed_search = 0;

  if( (d = (uint64_t *)calloc( sizeof(uint64_t),MAX_EVT)) == NULL) {
    printf("Memory allocation error\n");
    exit(1);
  }

  if( argc < 3) {
    usage();
    exit( 1);
  }

  for( int i=1; i<argc; i++) {
    if( *argv[i] == '-') {
      switch( toupper( argv[i][1])) {
      case 'F':
	if( i == argc-1) {
	  printf("Need FED number after -f\n");
	} else {
	  fed_search = atoi( argv[i+1]);
	  ++i;
	}
      }
    } else {
      if( num_files < max_files)
	files[num_files++] = argv[i];
    }
  }

//   if( !fed_search) {
//     printf("FED number must be specified with -f\n");
//     exit( 1);
//   }

  if( num_files != 2) {
    usage();
    exit( 1);
  }

  if( (fpi = fopen( files[0], "r")) == NULL) {
    printf("Error opening %s for input\n", files[0]);
    exit(1 );
  }

  if( (fpo = fopen( files[1], "w")) == NULL) {
    printf("Error opening %s for output\n", files[1]);
    exit(1);
  }

  while( !feof( fpi)) {
    if( !run_no) {		// don't already have a header
      if( fgets( buff, 255, fpi) == NULL)
	break;
      // should have header
      if( strncasecmp( buff, "Run ", 4)) {
	printf("Malformed header: %s\n", buff);
	exit( 1);
      }
      sscanf( buff, "%*s %d %*s %d", &run_no, &evn);
      printf("Found Run %d EvN %d\n", run_no, evn);
      nd = 0;
    }

    // read data until we see another header
    while( fgets( buff, 255, fpi) != NULL) {
      if( !strncasecmp( buff, "Run ", 4)) {
	sscanf( buff, "%*s %d %*s %d", &run_no, &evn);
	printf("Writing %d words\n", nd);
	hdr[0] = 0xbadc0ffeebadcafe;
	hdr[1] = nd;
	fwrite( hdr, sizeof(uint64_t), 2, fpo);
	fwrite( d, sizeof(uint64_t), nd, fpo);
	printf("Found Run %d EvN %d\n", run_no, evn);
	nd = 0;
	break;
      } else if( isdigit( *buff)) {
	sscanf( buff, "%*s %" SCNx64, &d[nd]);
	++nd;
      } else {
	printf("rubbish in file: %s\n", buff);
	exit( 1);
      }
    }
  }

}
