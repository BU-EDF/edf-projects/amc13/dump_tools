#include <stdlib.h>	    
#include <stdio.h>
#include <string.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdint.h>
#include <iostream>
#include <string>
#include <../unpacker/FedEvent.hh>
#include <tclap/CmdLine.h>
#include <fstream>
#include <histogram.hh>

#define N_AMC 12

// handle either "badcoffee" or "deadbeef" format

#define BADCOFFEE 0xbadc0ffeebadcafe
#define DEADBEEF 0xdeadbeef

int main( int argc, char *argv[]){
  
  std::vector<int> FEDID;
  int dumpLevel;
  bool CRCLevel;
  int errorLevel;
  std::vector<std::string> filename;
  uint32_t AMC13Errors[13][10] = {};
  uint64_t loopNumber=0;

  FedAMC13::Histogram histogram;
  std::vector<uint32_t> BcN;
  std::vector<uint32_t> OrN;
  size_t BcNLow;
  size_t BcNHigh;
  uint64_t OrNLow;
  uint64_t OrNHigh;


  try {
    TCLAP::CmdLine cmd("Tool for dumping AMC13 data.",
		       ' ',
		       "Dump Tool v1");
    
    //AMC 13 connections
    TCLAP::UnlabeledMultiArg<std::string> fileinput("f",         
						    "File Input",
						    true,
						    "string",         
						    cmd);

    //Dump Level Input
    TCLAP::ValueArg<int> dumpLevelInput("v", 
					"verbos",
					"Level of output: 1 for event info, 2" \
					"for event/block info, 3 for event/block/AMC" \
					"info, 4 for all + first 20 words",
					false,
					1,
					"integer",
					cmd);
    //CRC calc Level Input
    TCLAP::SwitchArg CRCLevelInput("c", 
				   "CRClevel",
				   "Level of CRC check: no input defaults to no check, -c for CRC check",
				   cmd,
				   false);

    //Error Checking
    TCLAP::ValueArg<int> errorLevelInput("e", 
					"errorLevel",
					"Level of output: 1 for event info, 2" \
					"for event/block info, 3 for event/block/AMC" \
					"info, 4 for all + first 20 words",
					false,
					0,
					"integer",
					cmd);
					
    cmd.parse(argc,argv);
    
    dumpLevel = dumpLevelInput.getValue();
    CRCLevel = CRCLevelInput.getValue();
    errorLevel = errorLevelInput.getValue();
    filename = fileinput.getValue();
    
  }catch (TCLAP::ArgException &e) {
    fprintf(stderr, "Error %s for arg %s\n",
	    e.error().c_str(), e.argId().c_str());
    return 0;
  }
  
  //Setup the Histograms before entering loop
  histogram.ConstructHistogram(0,FEDID);

  //File stream to read data files
  std::ifstream fileStream;
  BcN.clear();
  OrN.clear();


  //Loop over file inputs
  for(int iFile=0; iFile<filename.size();iFile++){

    //________________________File Input_______________________
    fileStream.open( filename[iFile].c_str(), std::ios::binary | std::ios::in);
    if( fileStream.fail()){
      std::cout<< "Error opening input file"<<filename[iFile]<<std::endl;
      exit(1);
    }
    std::cout<<"File "<<iFile+1<<":"<<filename[iFile]<<std::endl; 
    //_________________________________________________________




    //------------------------------Main Event Loop-------------------------

    //_________________________Initialize For Each File_________
    FedAMC13::FedEvent AMCEvent;
    loopNumber=0;
    //__________________________________________________________
    
    while(1){

      //_______________________Main Parsing Block________________
      //Clear previous event.
      AMCEvent.Clear();
      try{
	//If PreParse returns anything greater than 0 end of file has been reached.
	if(AMCEvent.PreParse(fileStream))
	  break;
	AMCEvent.ParseEvent();
	AMCEvent.Dump(dumpLevel,CRCLevel,errorLevel);
      }catch(FedException::exBase & e){
	std::cout << std::endl;
	std::cout << "Process " << e.GetPID() << " threw: " << e.what() << std::endl;
	std::cout << "Description: " << std::endl;
	std::cout << e.Description()<< std::endl;
	std::cout << "Stack trace:" << std::endl;
	std::cout << e.StackTrace()<< std::endl;
	exit(1);
      }
      //___________________________________________________________



      //______________________Print Statements and Histograms______
      try{
	//Printing Statements
	AMCEvent.Dump(dumpLevel,CRCLevel,errorLevel);
	
	AMCEvent.ErrorChecking();
	AMC13Errors[0][0]+=AMCEvent.GetAMC13Errors(0,0,0);

	//Histograms
	//Initializing bin ranges
	if(iFile==0 && loopNumber==0){
	  BcNHigh=AMCEvent.GetBcN();
	  BcNLow=AMCEvent.GetBcN();
	  OrNHigh=AMCEvent.GetOrN();
	  OrNLow=AMCEvent.GetOrN();	
	}
	//BcN and OrN are vectors that will go into the histogram object. 
	BcN.push_back(AMCEvent.GetBcN());
	OrN.push_back(AMCEvent.GetOrN());
	//Check if ranges need changing.
	if(AMCEvent.GetBcN() > BcNHigh)
	  BcNHigh = AMCEvent.GetBcN();
	if(AMCEvent.GetBcN() < BcNLow)
	  BcNLow = AMCEvent.GetBcN();      
	if(AMCEvent.GetOrN() > OrNHigh)
	  OrNHigh = AMCEvent.GetOrN();
	if(AMCEvent.GetOrN() < OrNLow)
	  OrNLow = AMCEvent.GetOrN(); 

	//Filling Error Histograms:      
	//printf("Loop: %u EvN:%lu Fill OrN: %lu \n",loopNumber,AMCEvent.GetEvN(),AMCEvent.GetOrN());
	for(uint32_t iAMC=1;iAMC<13;iAMC++){
	  for(uint32_t ierror=1;ierror<9;ierror++){
	    AMC13Errors[iAMC][ierror] += AMCEvent.GetAMC13Errors(iAMC,ierror,0);
	    
	    histogram.Fill_FEDErrors(AMCEvent.GetAMC13Errors(iAMC,ierror,0),1,iAMC,ierror);
	    //histogram.Fill_FEDErrors(1,1,iAMC,ierror);

	  }
	}   

      }catch(FedException::exBase & e){
	std::cout << std::endl;
	std::cout << "Process " << e.GetPID() << " threw: " << e.what() << std::endl;
	std::cout << "Description: " << std::endl;
	std::cout << e.Description()<< std::endl;
	std::cout << "Stack trace:" << std::endl;
	std::cout << e.StackTrace()<< std::endl;
	exit(1);
      }
      //___________________________________________________________

      loopNumber+=1;
      //------------------------------------End Main Event Loop-----------------------
    }

//____________________End of File Functions__________________
    fileStream.close();
    
    //Call the histogram filling functions
    histogram.Fill_BcN(BcN,BcNHigh,BcNLow);
    printf("Done with BcN\n");
    histogram.Fill_OrN(OrN,OrNHigh,OrNLow);
    printf("OrNHigh:%lu,OrNLow:%lu\n",OrNHigh,OrNLow);
    printf("BcNHigh:%lu,BcNLow:%lu\n",BcNHigh,BcNLow);
    BcN.clear();
    OrN.clear();
    
    //___________________________________________________________
    
  }

  //______________________End of File Loop________________________
  
  histogram.SetupHistogram(BcNLow,BcNHigh,OrNLow,OrNHigh);
  histogram.WriteHistogram();
  
//______________________________________________________________

  return 0;
}

  

