
/*
 * simple 64-bit word dump
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

main( int argc, char *argv[]) {
  FILE *fp;
  uint32_t v[2];
  int n;

  if( (fp = fopen( argv[1], "r")) == NULL) {
    printf("Can't open %s\n", argv[1]);
    exit(1);
  }

  n = 0;
  while( fread( v, sizeof(uint32_t), 2, fp) == 2) {
    printf("%4d (0x%04x): %08x %08x\n", n, n, v[1], v[0]);
    ++n;
  }
}
