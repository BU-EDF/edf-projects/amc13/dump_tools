DUMP_O=dump.o ../unpacker/FedBlock.o ../unpacker/FedEvent.o ../unpacker/FedAMC.o ../unpacker/Fedcrc16d64.o ../unpacker/FedException.o ../unpacker/FedException_StackTrace.o
DUMP=dump

DUMP64_O=dump64.o
DUMP64=dump64

UNDUMP_O = undump_FED_raw_data_Product.o
UNDUMP = undump_FED_raw_data_Product

UNDUMPH_O = undump_HCAL.o
UNDUMPH = undump_HCAL

DUMPOO=dump_over.o ../unpacker/FedBlock.o ../unpacker/FedEvent.o ../unpacker/FedAMC.o ../unpacker/Fedcrc16d64.o ../unpacker/FedException.o ../unpacker/FedException_StackTrace.o
DUMPO=dump_over

CXXFLAGS += -I./ -fno-strict-aliasing -g
LDFLAGS += -g

ifdef root
CXXFLAGS += $(shell root-config --cflags)
LDFLAGS += -L$(shell root-config --libdir) -lCore -lCint -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lMathCore -lThread -pthread -lm -ldl -rdynamic

DUMPTMP_O=dumptmp.o ../unpacker/FedBlock.o ../unpacker/FedEvent.o ../unpacker/FedAMC.o ../unpacker/Fedcrc16d64.o ../unpacker/FedException.o ../unpacker/FedException_StackTrace.o histogram.o
DUMPTMP=dumptmp
endif

all: $(DUMP) $(DUMP64) $(UNDUMP) $(UNDUMPH) $(DUMPTMP) $(DUMPO)

clean:
	@rm -f *~ \#* *.o ../unpacker/*.o $(DUMP) $(DUMP64) $(UNDUMP) $(UNDUMPH) $(DUMPTMP) $(DUMPO) > /dev/null

$(DUMPO):      $(DUMPOO)
	$(CXX) $(LDFLAGS) $^ -o $@ 

$(UNDUMP):	$(UNDUMP_O)
	$(CXX) $(LDFLAGS) $^ -o $@ 

$(UNDUMPH):	$(UNDUMPH_O)
	$(CXX) $(LDFLAGS) $^ -o $@ 

$(DUMP):	$(DUMP_O)
	$(CXX) $(LDFLAGS) $^ -o $@

$(DUMP64):	$(DUMP64_O)
	$(CXX) $(LDFLAGS) $^ -o $@

$(DUMPTMP):	$(DUMPTMP_O)
	$(CXX) $(LDFLAGS) $^ -o $@
