

stuff() {

  // example of how new unpacker might be used to illustrate the API

  uint64_t* data = ();  // magically get data from somewhere
  size_t nwords = ();   // get the size

  FedEvent* f = new FedEvent( data, nwords);      // construct an unpacker

  if( f->blocks.size() > 1)
    printf("Event is segmented with %d blocks\n", f->blocks.size());
  else
    printf("Event is unsegmented\n");

  // print lots of AMC information
  for( int i=0; i<f->nAMC(); i++) {
    int Slot = f->amc[i].Slot();              // get slot 1-12
    size_t Size = f->amc[i].Size();	      // get word count for AMC payload
    printf("AMC%d has %ld words\n", Slot, Size);
    // now dump the data
    for( int k=0; k<AmcSize; k++)
      printf("%5d: 0x%016llx\n", k, f->amc[i].Payload[k]); // get one 64-bit word
  }

  delete f;

}
