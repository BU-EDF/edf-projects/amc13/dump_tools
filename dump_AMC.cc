//
// Eric's handy event dumper
// handles either raw binary files for "Bad Coffee" files
//

#define NAMC 12

#include <stdlib.h>		// for NULL
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <iostream>
#include <string>
#include <../unpacker/FedEvent.hh>

char *fmt_bits( int v, int siz);

int main( int argc, char *argv[]){
  uint64_t size;
  uint64_t header[32];		// plenty big
  int level = 1;		// dump level

  FILE *fp;

  bool raw_binary;

  if( argc > 1)
    fp = fopen ( argv[1],"r");
  else {
    printf("usage:  %s <file>\n", argv[0]);
    exit( 1);
  }
  
  if(fp == NULL){
    printf("Error opening file %s\n", argv[1]);
    return 0;
  }
  
  long fpos;

  while( 1) {

    // remember file position
    fpos = ftell( fp);

    // read first 2 words
    if( fread( header, sizeof(uint64_t), 2, fp) != 2) {
      printf("End of File\n");
      exit( 0);
    }

    // deduce format
    if( header[0] == 0xbadc0ffeebadcafe) {
      size = header[1];
      printf("It's a \"Bad Coffee\" format file with size=%ld\n", size);
      raw_binary = false;
    } else if( ((header[0] >> 60) & 0xf) == 5) {
      printf("It might be a raw binary file...\n");
      size = 0;
      raw_binary = true;
    } else {
      printf("Unknown format, first word is 0x%016lx\n", header[0]);
      exit( 1);
    }
    
    if( raw_binary) {
      // read full header, create temporary unpacker, try to get size
      int nAMC = (header[1]>>52)&0xf;
      if( nAMC < 1 || nAMC > 12) {
	printf("Bad amc count = %d\n", nAMC);
	exit( 1);
      }
      printf("nAMC = %d\n", nAMC);
      // calculate the size

      // read AMC headers
      fread( &header[2], sizeof(uint64_t), nAMC, fp);
    
      int amc_siz[NAMC];
      int nblock[NAMC];
      int tsiz = 0;
      int tblk = 0;
      int nblock_max = 0;

      for( int i=0; i<nAMC; i++) {
	uint64_t ah = header[2+i];
	int lmsepvc = (ah>>56) & 0xff;
	amc_siz[i] = (ah>>32) & 0xffffff;
	tsiz += amc_siz[i];
	int no = (ah>>16) & 0xf;
	int blk = (ah>>20) & 0xff;
	// calculate block size
	if( amc_siz[i] <= 0x13fe)
	  nblock[i] = 1;
	else
	  nblock[i] = (amc_siz[i]-1023)/4096+1; // Wu's formula, fixed

	printf("Calculated block count %d from size %d using (siz-1023)/4096+1\n",
	       nblock[i], amc_siz[i]);
	tblk += nblock[i];
	if( nblock[i] > nblock_max)	// keep track of AMC with the most blocks
	  nblock_max = nblock[i];
	int bid = ah & 0xffff;
	printf( "%2d: %s %06x %02x  %04x\n", no, fmt_bits( lmsepvc, 8), amc_siz[i], blk, bid);
      }

      // calculate total event size Wu's way
      size = tsiz + tblk + nblock_max*2 + 2;
  
      printf("Calculated size 0x%lx by Wu formula:\n", size);
      printf("Nwords = sum(size) + sum(blocks) + NblockMax*2 + 2\n");
      printf("            %5d          %5d     %5d\n", tsiz, tblk, nblock_max);

    }

    
    //Create rawdata from rest of file:

    uint64_t* rawdata;
    rawdata = (uint64_t *)malloc( size * sizeof(uint64_t));

    if( raw_binary)
      fseek( fp, fpos, SEEK_SET);
      
    fread( rawdata, sizeof(uint64_t), size, fp);
  
    if( level > 1) {
      printf("First 20 words:\n");
      int i;
      for(i=0;i<=20;i++){
	printf("%04d 0x%016lx \n",i,rawdata[i]);
      }

      printf("Last 10 words:\n");
      for( i=size-10; i<size; i++) {
	printf("%04d 0x%016lx \n",i,rawdata[i]);
      }
    }

    //Unpacking Data
    FedAMC13::FedEvent amc13test;
    amc13test.ParseEvent(rawdata, size);

    //if(amc13test.B1_MS()==1

    // printf("Block: %08lx \n",amc13test.Blk(0));
  
    // check CRC
    uint16_t evt_crc = amc13test.CRC();
    uint16_t calc_crc = amc13test.CalcCRC();

    printf("Event CRC: %04x  Calculated CRC: %04x  %s\n", amc13test.CRC(), amc13test.CalcCRC(),
	   (evt_crc != calc_crc) ? "ERROR" : "OK");



    free(rawdata);
  }


}



  

char *fmt_bits( int v, int siz)
{
  static char s[64];
  if( siz < 1 || siz > 64) {
    printf("Invalid size %d in fmt_bits\n", siz);
    exit(1);
  }
  for( int i=0; i<siz; i++)
    s[i] = (v >> (siz-i-1)) & 1 ? '1' : '0';
  s[siz] = '\0';
  return s;
}

