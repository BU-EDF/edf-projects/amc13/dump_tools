Instructions to build tools to unpack data files from the AMC13. 
Dan Arcaro (djarcaro@bu.edu)
Last Updated: 3/25/15

Check out the required code:
$ svn co http://edf.bu.edu/svn/edf/arcaro/tools/
$ svn co http://edf.bu.edu/svn/edf/arcaro/unpacker/
$ cd tools/
$ make


How to make a histogram with the Histogram class:

include "../analyzer/parse/plugins/Histogram.hh"

int main(){
  std::vector<int> FEDID;
  FedAMC13::Histogram histogram;
  histogram.SetupHistogram(0,FEDID);
  for(int i=0;i<100;i++){
    histogram.Fill_placeholder(i);
  }
  for(int i=0;i<50;i++){
    histogram.Fill_placeholder(i);
  }
  histogram.WriteHistogram();
}
