#include <stdlib.h>	    
#include <stdio.h>
#include <string.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdint.h>
#include <iostream>
#include <string>
#include <../unpacker/FedEvent.hh>
#include <tclap/CmdLine.h>
#include <fstream>

#define N_AMC 12

// handle either "badcoffee" or "deadbeef" format

#define BADCOFFEE 0xbadc0ffeebadcafe
#define DEADBEEF 0xdeadbeef

int main( int argc, char *argv[]){
  
  std::vector<int> FEDID;
  int dumpLevel;
  bool checkCRC;
  int errorLevel;
  std::vector<std::string> filename;
  std::string histitem;
  uint32_t AMC13Errors[13][10] = {};
  uint64_t evt_tyme = 0;
  uint64_t loopNumber=0;

  try {
    TCLAP::CmdLine cmd("Tool for dumping AMC13 data.",
		       ' ',
		       "Dump Tool v1");
    
    //AMC 13 connections
    TCLAP::UnlabeledMultiArg<std::string> fileinput("f",         
						    "File Input",
						    true,
						    "string",         
						    cmd);
    
    // Output values for histogramming
    TCLAP::ValueArg<std::string> histItemInput("i",
					       "HistogramItem",
					       "Specify item for histo:\n" \
					       "\t b - BcN\n" \
					       "\t o - OrN",
					       false,
					       "",
					       "string",
					       cmd);
    //Dump Level Input
    TCLAP::ValueArg<int> dumpLevelInput("v", 
					"verbos",
					"Level of output: 1 for event info, 2" \
					"for event/block info, 3 for event/block/AMC" \
					"info, 4 for all + first 20 words",
					false,
					1,
					"integer",
					cmd);

    //CRC chceck enable
    TCLAP::SwitchArg checkCRCInput("c", 
				   "checkCRC",
				   "Check CRC in software",
				   cmd,
				   false);

    //Error Checking
    TCLAP::ValueArg<int> errorLevelInput("e", 
					"errorLevel",
					"Level of output: 1 for event info, 2" \
					"for event/block info, 3 for event/block/AMC" \
					"info, 4 for all + first 20 words",
					false,
					0,
					"integer",
					cmd);
					
    cmd.parse(argc,argv);

    dumpLevel = dumpLevelInput.getValue();
    checkCRC = checkCRCInput.getValue();
    errorLevel = errorLevelInput.getValue();
    filename = fileinput.getValue();
    histitem = histItemInput.getValue();
    
  }catch (TCLAP::ArgException &e) {
    fprintf(stderr, "Error %s for arg %s\n",
	    e.error().c_str(), e.argId().c_str());
    return 0;
  }
  


  //File stream to read data files
  std::ifstream fileStream;

  //Loop over file inputs
  for(int iFile=0; iFile<filename.size();iFile++){

    //________________________File Input_______________________
    fileStream.open( filename[iFile].c_str(), std::ios::binary | std::ios::in);
    if( fileStream.fail()){
      std::cout<< "Error opening input file"<<filename[iFile]<<std::endl;
      exit(1);
    }
    std::cout<<"File "<<iFile+1<<":"<<filename[iFile]<<std::endl; 
    //_________________________________________________________




    //------------------------------Main Event Loop-------------------------

    //_________________________Initialize For Each File_________
    FedAMC13::FedEvent AMCEvent;
    loopNumber=0;
    //__________________________________________________________
    


    while(1){

      //_______________________Main Parsing Block________________
      //Clear previous event.
      AMCEvent.Clear();

      try{
	//If PreParse returns anything greater than 0 end of file has been reached.
	if(AMCEvent.PreParse(fileStream))
	  break;
	AMCEvent.ParseEvent();
      }catch(FedException::exBase & e){
	std::cout << std::endl;
	std::cout << "Process " << e.GetPID() << " threw: " << e.what() << std::endl;
	std::cout << "Description: " << std::endl;
	std::cout << e.Description()<< std::endl;
	std::cout << "Stack trace:" << std::endl;
	std::cout << e.StackTrace()<< std::endl;
	exit(1);
      }
      //___________________________________________________________


      
      //______________________Print Statements_____________________
      try{
	AMCEvent.Dump(dumpLevel,checkCRC,errorLevel);
	
	AMCEvent.ErrorChecking();
	AMC13Errors[0][0]+=AMCEvent.GetAMC13Errors(0,0,0);
	if( histitem == "b")
	  printf("%ld\n", AMCEvent.GetBcN());
	else if( histitem == "o")
	  printf("%ld\n", AMCEvent.GetOrN());
	else if( histitem == "bo") {
	  uint64_t tyme = (uint64_t)AMCEvent.GetOrN() * 3564L + AMCEvent.GetBcN();
	  if( evt_tyme)
	    printf("%" PRIu64 "\n", tyme-evt_tyme);
	  evt_tyme = tyme;
	}
     
	for(uint32_t iAMC=1;iAMC<13;iAMC++){
	  for(uint32_t ierror=1;ierror<9;ierror++){
	    AMC13Errors[iAMC][ierror] += AMCEvent.GetAMC13Errors(iAMC,ierror,0);
	  }
	}   

      }catch(FedException::exBase & e){
	std::cout << std::endl;
	std::cout << "Process " << e.GetPID() << " threw: " << e.what() << std::endl;
	std::cout << "Description: " << std::endl;
	std::cout << e.Description()<< std::endl;
	std::cout << "Stack trace:" << std::endl;
	std::cout << e.StackTrace()<< std::endl;
	exit(1);
      }
      //___________________________________________________________
      


      loopNumber+=1;
      //------------------------------------End Main Event Loop-----------------------
    }
   
    //____________________End of File Functions__________________
    fileStream.close();
    //___________________________________________________________
    
  }

  //______________________End of File Loop________________________
  //______________________________________________________________

  return 0;
}

  

