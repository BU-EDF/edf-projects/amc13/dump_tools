//
// read output from CMSSW analyzer "DumpFEDRawDataProduct"
// write as binary deadbeef file
//
// usage:  
//

#include <stdio.h>
#include <stdint.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#define MAX_EVT (1<<20)

void usage() {
  printf("usage:  undump_FED_raw_data_Product -f <fed> <hex_file> <bin_file\n");
}

int main( int argc, char *argv[]) {

  FILE *fpi, *fpo;
  char buff[256];
  uint64_t *d;
  int nd = 0;
  int fed_no, fed_size, evn, bcn;
  int fed_search = 0;

  const int max_files = 2;
  char *files[max_files];
  int num_files = 0;

  if( (d = (uint64_t *)calloc( sizeof(uint64_t),MAX_EVT)) == NULL) {
    printf("Memory allocation error\n");
    exit(1);
  }

  if( argc < 3) {
    usage();
    exit( 1);
  }

  for( int i=1; i<argc; i++) {
    if( *argv[i] == '-') {
      switch( toupper( argv[i][1])) {
      case 'F':
	if( i == argc-1) {
	  printf("Need FED number after -f\n");
	} else {
	  fed_search = atoi( argv[i+1]);
	  ++i;
	}
      }
    } else {
      if( num_files < max_files)
	files[num_files++] = argv[i];
    }
  }

  if( !fed_search) {
    printf("FED number must be specified with -f\n");
    exit( 1);
  }

  if( num_files != 2) {
    usage();
    exit( 1);
  }

  if( (fpi = fopen( files[0], "r")) == NULL) {
    printf("Error opening %s for input\n", files[0]);
    exit(1 );
  }

  if( (fpo = fopen( files[1], "w")) == NULL) {
    printf("Error opening %s for output\n", files[1]);
    exit(1);
  }

  while( fgets( buff, 255, fpi) != NULL) {

    // look for event header
    if( !strncmp( buff, "FED#", 4)) {

      // end of previous event?
      if( nd && fed_no == fed_search) {

	if( nd != fed_size/8) {
	  printf("Header reported %d bytes, counted %d words (%d bytes)\n",
		 fed_size, nd, nd*8);
	  exit( 1);
	}
	uint32_t hdr[2];
	hdr[0] = 0xdeadbeef;
	hdr[1] = nd * 2;
	fwrite( hdr, sizeof(hdr[0]), 2, fpo);
	fwrite( d, sizeof(d[0]), nd, fpo);
      }

      sscanf( buff, "%*s %d %d %*s %*s %d %*s %d", &fed_no, &fed_size, &evn, &bcn);
      printf("FED %d size %d evn %d bcn %d\n", fed_no, fed_size, evn, bcn);
      nd = 0;
    } else if( isxdigit( *buff)) {
    // collect the data
      sscanf( buff, "%*s %" SCNx64, &d[nd]);
      if( ++nd > MAX_EVT) {
	printf("Event too long!\n");
	exit(1);
      }
    }
  }

  // end of previous event?
  if( nd && fed_no == fed_search) {
    if( nd != fed_size/8) {
      printf("Header reported %d bytes, counted %d words (%d bytes)\n",
	     fed_size, nd, nd*8);
      exit( 1);
    }
    uint32_t hdr[2];
    hdr[0] = 0xdeadbeef;
    hdr[1] = nd * 2;
    fwrite( hdr, sizeof(hdr[0]), 2, fpo);
    fwrite( d, sizeof(d[0]), nd, fpo);
  }



}
