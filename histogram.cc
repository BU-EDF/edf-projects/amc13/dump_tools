#include "histogram.hh"

namespace FedAMC13{
  
  Histogram::Histogram(){  
  }

  Histogram::~Histogram(){
  }

  //Enter in any information from {parse} that is needed in labels
  void Histogram::ConstructHistogram(int runNumber,std::vector<int> FEDID){ 
    //Create the root file
    char tmpName[50];
    snprintf( tmpName, 50, "Histo%06d.root", runNumber);
    rootFileName = tmpName;    
    rootFile = new TFile(rootFileName.c_str(), "recreate");
    //Constructors for histograms:
   
    //histoBcN = new TH1D("BcN","BcN",100,0,0);
    //histoOrN = new TH1D("OrN","OrN",5000,0,0);
    FEDErrors = new TH2D("FEDErrors" , "FEDErrors", 12*5 +1 , 0 , 12*5+1, 12*5 +2 , 0 , 12*5+2);
    //Labels:
    const char* ylabel[12] = {"AMC1","AMC2","AMC3","AMC4","AMC5","AMC6","AMC7","AMC8","AMC9","AMC10","AMC11","ACM12"}; 
    TAxis *yaxis = FEDErrors->GetYaxis();
    TAxis *xaxis = FEDErrors->GetXaxis();
    for( uint32_t iy=0; iy<12; iy++) {
      yaxis->SetBinLabel(iy*5+3, ylabel[iy]);
    }
    char tmpLabel[20];
    for(uint32_t ix=0; ix<FEDID.size(); ix++) {
      sprintf(tmpLabel, "FED %3d",FEDID[ix]);
      xaxis->SetBinLabel( ix*3+4, tmpLabel); 
    } 
  }

  void Histogram::SetupHistogram(size_t BcNLow, size_t BcNHigh, uint32_t OrNLow, uint32_t OrNHigh){
    //histoBcN->GetXaxis()->SetLimits(BcNLow,BcNHigh);
    //histoBcN->GetXaxis()->SetLimits(450,2100);
    //histoBcN->RebinAxis((BcNHigh-BcNLow)/2,histoBcN->GetXaxis());
   
    //histoBcN->GetXaxis()->SetRangeUser(BcNLow,BcNHigh);
    //histoBcN->RebinAxis(10,histoBcN->GetXaxis());
   
    //histoOrN->GetXaxis()->SetRangeUser(OrNLow,OrNHigh);
    //histoOrN->RebinAxis((OrNHigh-OrNLow)/2,histoOrN->GetXaxis());
  }
    
  void Histogram::WriteHistogram(){
    rootFile->WriteObject(histoBcN,histoBcN->GetName());
    rootFile->WriteObject(histoOrN,histoOrN->GetName());
    rootFile->WriteObject(FEDErrors,FEDErrors->GetName());
    rootFile->Close();
  }
  
  //Below is a list of Fill function for individual graphs
  void Histogram::Fill_BcN(std::vector<uint32_t> BcN,uint32_t BcNHigh,uint32_t BcNLow){
    histoBcN = new TH1D("BcN","BcN",(BcNHigh-BcNLow)/10,BcNLow,BcNHigh);
    for(int i=0;i<BcN.size();i++){
      histoBcN->Fill(BcN[i]);
    }
  }
  void Histogram::Fill_OrN(std::vector<uint32_t> OrN,uint32_t OrNHigh,uint32_t OrNLow){
    histoOrN = new TH1D("OrN","OrN",(OrNHigh-OrNLow)/100,OrNLow,OrNHigh);
    for(int i=0;i<OrN.size();i++){
      histoOrN->Fill(OrN[i]);
    }
  }
  void Histogram::Fill_FEDErrors(uint32_t errorState, uint32_t iFED, uint32_t iAMC, uint32_t ierror){
    
    if(errorState > 0){
      uint32_t x = (iFED*3)+3;
      if(ierror > 4){
	x++;
      }
      uint32_t y = ((iAMC-1)*5+1);
      switch(ierror){
      case 1:
      case 8:
	y+=1;
	break;  
      case 2:
      case 7:
	y+=2;
	break;
      case 3:
      case 6:
	y+=3;
	break;
      case 4:
      case 5:
	y+=4;
	break;
      }
      FEDErrors -> Fill(x,y);
    }   
  }
}
