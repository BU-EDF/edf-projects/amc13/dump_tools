/*
 * dump an AMC13 file from the experimental "monitor buffer overwrite" feature
 *
 * file format:
 * 0xdeadfacefeedbeef
 *   <word_count>   (32-bit word count)
 *   <event_1>
 * 0xdeadfacefeedbeef
 *   <word_count>   (32-bit word count)
 *   <event_2>
 *
 * Events are raw memory blocks from monitor buffer. 
 * Note that unlike the usual data format the size is just
 * the size that the user chose to dump and is probably unrelated
 * to the actual block/event size, so the main program reads
 * each "event" and tries to unpack it separately.
 *
 * This whole scheme will likely fail on multi-block events;
 * some additional work will be needed to handle them.
 */

#include <stdlib.h>	    
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <iterator>

#include <../unpacker/FedEvent.hh>

using std::cout;
using std::cerr;
using std::endl;
using std::vector;

#define MAGIC 0xdeadfacefeedbeef

// helper function to do a binary read from a file into a 64-bit vector
void VectorRead64( std::ifstream &s, int n, vector<uint64_t> &v);

enum file_type_t { BadCoffee, RawBinary, UnknownType };


int main( int argc, char *argv[])
{
  FedAMC13::FedEvent AMCEvent;
  vector<uint64_t> head;
  std::ifstream fs;
  int dumpLevel = 1;
  int checkCRC = 1;
  int errorLevel = 4;

  fs.open( argv[1], std::ios::binary | std::ios::in);
  if( fs.fail()){
    std::cout<< "Error opening input file" << argv[1] << std::endl;
    exit(1);
  }

  while(1){

    AMCEvent.Clear();


    // read header
    VectorRead64( fs, 2, head);

    int posn = fs.tellg();

    if( head[0] != MAGIC) {
      printf("Bad header: %016lx %016lx\n", head[0], head[1]);
      exit(1);
    }

    try{
      //If PreParse returns anything greater than 0 end of file has been reached.
      if(AMCEvent.PreParse(fs))
	break;
      AMCEvent.ParseEvent();
    } catch(FedException::exBase & e) {
      std::cout << std::endl;
      std::cout << "Process " << e.GetPID() << " threw: " << e.what() << std::endl;
      std::cout << "Description: " << std::endl;
      std::cout << e.Description()<< std::endl;
      exit(1);
    }

    AMCEvent.Dump(dumpLevel,checkCRC,errorLevel);
    AMCEvent.ErrorChecking();

    fs.seekg(posn + head[1]*4  );

  }

}




//
// read n uint64_t from stream append to a vector and return it
// errors to stderr and exit
//
void VectorRead64( std::ifstream &s, int n, vector<uint64_t> &v)
{
  vector<uint64_t> vt(n);
  s.read( (char *)vt.data(), n * sizeof(uint64_t));

  if( !s.good()) {
    cerr << "Error attempting to read " << n << " words from input" << endl;
    exit(1);
  }

  v.insert( v.end(), vt.begin(), vt.end());
}
