/*
 * dump an AMC13 data file
 *
 * CMS-standard format starts each event with 64-bit magic number 0xbadc0ffeebadcafe
 * followed by a count of 64-bit words.  In this case we double-check the word count
 * against the unpacker-calculated value
 *
 * We can also handle "raw" binary format where the events are stored with no
 * delimiters and hope for the best
 */

#include <stdlib.h>	    
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <iterator>

#include <FedEvent.hh>
#include <tclap/CmdLine.h>

using std::cout;
using std::cerr;
using std::endl;
using std::vector;


// CMS file sentinel value
#define BADCOFFEE 0xbadc0ffeebadcafe

// helper function to do a binary read from a file into a 64-bit vector
void VectorRead64( std::ifstream &s, int n, vector<uint64_t> &v);

enum file_type_t { BadCoffee, RawBinary, UnknownType };


int main( int argc, char *argv[])
{
  int dumpLevel;
  std::string FileName;
  file_type_t FileType = UnknownType;
  bool EnableCRC;

  // Ok, ok, I'll try this tclaptrap argument thing....
  try {
    TCLAP::CmdLine cmd("Eric's Tool for dumping AMC13 data.", ' ', "2.0");
    
    TCLAP::UnlabeledValueArg<std::string>
      fileinput("f","input file",true,"","File name", cmd);

    TCLAP::ValueArg<int>
      dumpLevelInput("v", "verbosity", "Dump level (details?)", false, 1, "integer", cmd);

    TCLAP::SwitchArg CRCEnableInput( "c", "crc_check", "Enable CRC check", cmd, false);

    cmd.parse(argc,argv);
    
    // get the parsed argument values
    FileName = fileinput.getValue();
    dumpLevel = dumpLevelInput.getValue();
    EnableCRC = CRCEnableInput.getValue();

  } catch (TCLAP::ArgException &e) {
    fprintf(stderr, "Error %s for arg %s\n",
	    e.error().c_str(), e.argId().c_str());
    return 0;
  }
  
  // open the data file
  std::ifstream inStr;
  inStr.open( FileName.c_str(), std::ios::binary | std::ios::in);
  if( !inStr.good()) {
    cerr << "Error opening input file " << FileName << endl;
    exit(1);
  }

  // read first two words from file to see if there is a CMS header
  vector<uint64_t> fileHeader;
  VectorRead64( inStr, 2, fileHeader);

  // figure out the file type
  if( fileHeader[0] == BADCOFFEE) {
    FileType = BadCoffee;
    fprintf( stderr, "It's a BadCoffee format file\n");
  } else if( ((fileHeader[0] >> 60) & 0xf) == 5) {
    FileType = RawBinary;
    fprintf( stderr, "It looks like a raw binary file\n");
  } else {
    fprintf( stderr, "Unknown file type, first word = 0x%016" PRIx64 "\n", fileHeader[0]);
    exit(1);
  }
  
  // back to start of the file
  inStr.seekg(0);

  // allocate an empty vector for the data
  vector<uint64_t> eventData;

  // Create a blank FedEvent object, which gets re-used
  FedAMC13::FedEvent AMCEvent;

  // now we're ready to loop over events
  while( inStr.good()) {

    printf("--- Reading Event ---\n");

    // read, check and skip header if any
    if( FileType == BadCoffee) {
      fileHeader.clear();
      VectorRead64( inStr, 2, fileHeader);
      if( fileHeader[0] != BADCOFFEE) {
	cerr << "Bad magic number in start of event 0x" << std::hex << fileHeader[0] << endl;
	exit(1);
      }
    }

    // now we're ready to read the event, using the preparse method to get the size
    AMCEvent.Clear();
    int nAMC;
    int wordsRead;

    // read the event
    // this is complicated because we don't know how big it is
    // the complexity should be in a function somewhere eventually
    try {
      eventData.clear();		                           // make sure the vector is empty
      VectorRead64( inStr, 2, eventData);                          // read first 2 words
      nAMC = AMCEvent.GetNumAMC( eventData.data());	           // get# number of AMC
      VectorRead64( inStr, nAMC, eventData);                       // read remainder of header
      wordsRead = nAMC+2;					   // keep track of words read
      int tSize = AMCEvent.GetEventSize( eventData.data());        // calculate full event size
      VectorRead64( inStr, tSize, eventData);			   // read rest of event
      wordsRead += tSize;
    } catch(FedException::exBase & e){
      printf("%s",e.what());
      printf("%s",e.Description());
    }

    // Check if badc0ffee format size is equal to the calculated size
    if( FileType == BadCoffee) {
      if(wordsRead!=fileHeader[1]) {
	printf("badc0ffee format size does not agree with calculated event size\n");
	exit(1);
      }
    }
    
    //Make FedEvent object and check if parsing went ok:
    try{
      AMCEvent.ParseEvent(eventData.data(), wordsRead);
    }catch(FedException::exBase & e){
      std::cout << std::endl;
      std::cout << "Process " << e.GetPID() << " threw: " << e.what() << std::endl;
      std::cout << "Description: " << std::endl;
      std::cout << e.Description()<< std::endl;
      std::cout << "Stack trace:" << std::endl;
      std::cout << e.StackTrace()<< std::endl;
      exit(1);
    }

    printf("Dumping level %d\n", dumpLevel);
    AMCEvent.Dump( dumpLevel, EnableCRC);

    // access the data payloads
    if( dumpLevel > 2) {
      cout << "---> Dumping the payloads" << endl;
      int namc = AMCEvent.GetNAMC();
      cout << "  Number of AMCs: " << namc << endl;
       for( int i=0; i<namc; i++) {
	 // NOTE GetAMC(1..12) not (0..11)
	 int amcNO = AMCEvent.GetAMC(i+1).GetAMCNumber();
	 int amcSize = AMCEvent.GetAMC(i+1).GetSize();
	 cout << "AMC " << amcNO << " size=" << amcSize << endl;
	 for( int k=0; k<amcSize; k++)
	   cout << std::setw(8) << std::dec << k
		<< " 0x" << std::setw(16) << std::hex << std::setfill('0')
		<< AMCEvent.GetAMC(i+1).GetWord(k) << endl;
       }
    }
  }


}




//
// read n uint64_t from stream append to a vector and return it
// errors to stderr and exit
//
void VectorRead64( std::ifstream &s, int n, vector<uint64_t> &v)
{
  vector<uint64_t> vt(n);
  s.read( (char *)vt.data(), n * sizeof(uint64_t));

  if( !s.good()) {
    cerr << "Error attempting to read " << n << " words from input" << endl;
    exit(1);
  }

  v.insert( v.end(), vt.begin(), vt.end());
}
